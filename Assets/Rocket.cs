﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Rocket : MonoBehaviour {
    new Rigidbody rigidbody;
    AudioSource audioSource;
    
    enum State {
        Alive,
        Dying,
        Transcending
    }

    [SerializeField] float rcsThrust = 100f;
    [SerializeField] float mainThrust = 100f;
    [SerializeField] State state = State.Alive;
    [SerializeField] float LevelTransitionDelay = 1f;
    [SerializeField] AudioClip mainEngine;
    [SerializeField] AudioClip explosion;
    [SerializeField] AudioClip successChime;
    [SerializeField] ParticleSystem mainEngineParticle;
    [SerializeField] ParticleSystem explosionParticle;
    [SerializeField] ParticleSystem successParticle;

    private bool checkCollisions = true;

    // Use this for initialization
    void Start () {
        rigidbody = GetComponent<Rigidbody>();
        audioSource = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {
        if (state == State.Alive) {
            HandleThrustInput();
            HandleRotationInput();
        }
        HandleDebugKeys();
    }

    private void HandleDebugKeys() {
        if (Debug.isDebugBuild) {
            if (Input.GetKeyDown(KeyCode.C)) {
                checkCollisions = !checkCollisions;
            }
            if (Input.GetKeyDown(KeyCode.L)) {
                LoadNextScene();
            }
        }
    }

    void OnCollisionEnter(Collision collision) {
        if (state == State.Alive && checkCollisions) {
            switch (collision.gameObject.tag) {
                case "Friendly":
                    // do nothing
                    break;
                case "Finish":
                    ExecuteSuccessSequence();
                    break;
                default:
                    ExecuteDeathSequence();
                    break;
            }
        }
    }

    private void ExecuteSuccessSequence() {
        state = State.Transcending;
        audioSource.Stop();
        audioSource.PlayOneShot(successChime);
        successParticle.Play();
        Invoke("LoadNextScene", LevelTransitionDelay);
    }

    private void ExecuteDeathSequence() {
        state = State.Dying;
        audioSource.Stop();
        audioSource.PlayOneShot(explosion);
        explosionParticle.Play();
        Invoke("StartGameOver", LevelTransitionDelay);
    }

    private void StartGameOver() {
        SceneManager.LoadScene(0);
    }

    private void LoadNextScene() {
        int currIndex = SceneManager.GetActiveScene().buildIndex;
        int nextIndex = (SceneManager.sceneCountInBuildSettings > (currIndex + 1)) ? (currIndex + 1) : 0;
        SceneManager.LoadScene(nextIndex);
        
    }

    private void HandleThrustInput() {
        if (Input.GetKey(KeyCode.Space)) {
            ApplyThrust();
            if (!audioSource.isPlaying) {
                audioSource.PlayOneShot(mainEngine);
            }
            mainEngineParticle.Play();
        } else {
            audioSource.Stop();
            mainEngineParticle.Stop();
        }
    }

    private void ApplyThrust() {
        float thrustThisFrame = mainThrust * Time.deltaTime;
        rigidbody.AddRelativeForce(Vector3.up * thrustThisFrame);
    }

    private void HandleRotationInput() {
        
        rigidbody.angularVelocity = Vector3.zero; //take manual control of rot

        float rotationThisFrame = rcsThrust * Time.deltaTime;
        if (Input.GetKey(KeyCode.LeftArrow)) {
            transform.Rotate(Vector3.forward * rotationThisFrame);
        } else if (Input.GetKey(KeyCode.RightArrow)) {
            transform.Rotate(-Vector3.forward * rotationThisFrame);
        }
    }
}
